//
//  PlantsAppApp.swift
//  PlantsApp
//
//  Created by Mohammad Azam on 9/28/20.
//

import SwiftUI

@main
struct PlantsAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
