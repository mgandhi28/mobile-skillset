//
//  MusicAppApp.swift
//  MusicApp
//
//  Created by Monil Gandhi on 20/11/20.
//

import SwiftUI

@main
struct MusicAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
