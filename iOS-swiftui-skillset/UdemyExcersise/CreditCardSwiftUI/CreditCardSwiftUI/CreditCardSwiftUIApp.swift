//
//  CreditCardSwiftUIApp.swift
//  CreditCardSwiftUI
//
//  Created by Monil Gandhi on 25/11/20.
//

import SwiftUI

@main
struct CreditCardSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
