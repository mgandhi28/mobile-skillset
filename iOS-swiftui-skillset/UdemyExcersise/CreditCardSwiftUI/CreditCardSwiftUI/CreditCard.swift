//
//  CreditCard.swift
//  CreditCardSwiftUI
//
//  Created by Monil Gandhi on 25/11/20.
//

import SwiftUI

struct CreditCard<Content>: View where Content: View {
    
    var content: () -> Content
    
    var body: some View {
        content()
    }
}

struct CreditCardFront: View {
    
    let name: String
    let expires: String
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack(alignment: .top) {
                Image(systemName: "checkmark.circle.fill")
                    .foregroundColor(.white)
                
                Spacer()
                
                Text("VISA")
                    .foregroundColor(.white)
                    .font(.system(size: 24))
                    .fontWeight(.bold)
            }
            
            Spacer()
            
            Text("**** **** **** 2864")
                .foregroundColor(.white)
                .font(.system(size: 32))
            
            Spacer()
            
            
            HStack {
                VStack(alignment: .leading) {
                    Text("CARD HOLDER")
                        .foregroundColor(.gray)
                        .fontWeight(.bold)
                        .font(.caption)
                    
                    Text(name)
                        .foregroundColor(.white)
                        .fontWeight(.bold)
                        .font(.caption)
                }
                
                Spacer()
                
                VStack {
                    Text("EXPIRES")
                        .foregroundColor(.gray)
                        .fontWeight(.bold)
                        .font(.caption)
                    
                    Text(expires)
                        .foregroundColor(.white)
                        .fontWeight(.bold)
                        .font(.caption)
                }
            }
        }
        .padding()
        .frame(width: 300, height: 200)
        .background(LinearGradient(gradient: Gradient(colors: [Color(#colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)), Color.blue]), startPoint: .topLeading, endPoint: .bottomTrailing))
        .cornerRadius(10.0)
    }
}

struct CreditCardBack: View {
    
    let cvv: String
    
    var body: some View {
        VStack {
            Rectangle()
                .frame(maxWidth: .infinity, maxHeight: 20)
                .padding([.top])
            
            Spacer()
            
            HStack {
                Text(cvv)
                    .foregroundColor(.black)
                    .rotation3DEffect(
                        .degrees(180),
                        axis: (x: 0.0, y: 1.0, z: 0.0)
                    )
                    .padding(5)
                    .frame(width: 100, height: 20)
                    .background(Color.white)
                
                Spacer()
                
            }.padding()
        }.frame(width: 300, height: 200)
        .background(LinearGradient(gradient: Gradient(colors: [Color(#colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)), Color.blue]), startPoint: .topLeading, endPoint: .bottomTrailing))
        .cornerRadius(10.0)
    }
}

struct CreditCard_Previews: PreviewProvider {
    static var previews: some View {
        CreditCard<CreditCardFront>(content: { CreditCardFront(name: "Monil Gandhi", expires: "02/23")})
    }
}
